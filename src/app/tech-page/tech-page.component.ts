import { Component, OnInit, Input } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'tech-page',
  templateUrl: './tech-page.component.html',
  styleUrls: ['./tech-page.component.css']
})
export class TechPageComponent implements OnInit {
  @Input() language : String;

  constructor(private translate: TranslateService) { }

  ngOnInit() {
  }

}
