import { MailServiceService } from './../mail-service.service';
import { Component, OnInit, Input } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { NgForm } from '@angular/forms';

@Component({
  selector: 'contact-page',
  templateUrl: './contact-page.component.html',
  styleUrls: ['./contact-page.component.css']
})
export class ContactPageComponent implements OnInit {
  loading = false;

  sendit = false;

  @Input() language : String;


  constructor( private ms : MailServiceService, private translate: TranslateService) { }


  ngOnInit() {
  }

  contacto(form: NgForm) {
    form.value.contact_type = "Contacto";
    console.log(form.value);
    this.loading = true;
    this.ms.sendContact(form.value).subscribe(
      data => { 
        this.loading = false;
        form.reset();
        this.sendit = true;        
      },
      error => {
        this.loading = false;
        alert("Intente mas tarde");
      }
    );
  }

}
