import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  defaultIdioma = 'en';
  title = 'app';

  constructor(private translate: TranslateService) { 
    this.defaultIdioma = window.navigator.language.slice(0,2); 
    if(this.defaultIdioma != 'es' && this.defaultIdioma != 'en')
      this.defaultIdioma = 'en';
    
    this.change(this.defaultIdioma);
    
  }

  ngOnInit() {
    this.translate.setDefaultLang(this.defaultIdioma);
  }

  change(idioma){
    console.log(idioma);
    
    this.defaultIdioma = idioma;
    
    this.translate.use(idioma);

    
  }

}
