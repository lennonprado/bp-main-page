import { Component, OnInit, Input } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'about-page',
  templateUrl: './about-page.component.html',
  styleUrls: ['./about-page.component.css']
})



export class AboutPageComponent implements OnInit {

  @Input() language : String;

  constructor(private translate: TranslateService) { }

  ngOnInit() {}


  showArgentina(){
    
    if(this.language == 'es')
      return false;
    else
      return true;  
  }

}
