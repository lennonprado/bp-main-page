import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class MailServiceService {

  constructor(private httpClient: HttpClient) { }


  sendContact(postData: any){
    
    postData.subject = "Contacto desde la web de bp sistemas";
    postData.to = "info@bpsistemas.com";

    return this.httpClient.post("http://api.clientes.bpsistemas.com/email",postData);
  }


}



 